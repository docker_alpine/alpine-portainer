# alpine-portainer
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-portainer)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-portainer)

### x64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-portainer/x64)
### aarch64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-portainer/aarch64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64
* Appplication : Portainer
    - Portainer is a lightweight management UI which allows you to easily manage your different Docker environments (Docker hosts or Swarm clusters).



----------------------------------------
#### Run

```sh
docker run -d \
           -p 8900:8900/tcp \
           -v /repo:/repo \
		   -v /var/run/docker.sock:/var/run/docker.sock \
           forumi0721/alpine-portainer:[ARCH_TAG]
```



----------------------------------------
#### Usage

* URL : [http://localhost:8900/](http://localhost:8900/)



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 8900/tcp           | HTTP port                                        |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /repo              | Persistent data                                  |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

