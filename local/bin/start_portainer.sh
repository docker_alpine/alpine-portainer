#!/bin/sh

set -e

exec /app/portainer/portainer --bind=":8900" --data="/repo"

